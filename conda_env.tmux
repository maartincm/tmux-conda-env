#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


tmux bind-key O run-shell "$CURRENT_DIR/scripts/tmux_current_conda_env_as_default.sh '#{pane_id}' '#{session_id}'"
