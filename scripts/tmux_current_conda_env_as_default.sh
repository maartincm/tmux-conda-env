#!/usr/bin/env bash

pane_id="$1"
session_id="$2"

tmux command-prompt -p "(Conda Env)" -I "$CONDA_DEFAULT_ENVIRONMENT" " \
if-shell -b \"cd ~/.conda/envs/%1\" \" \
    setenv -t ${session_id} CONDA_DEFAULT_ENVIRONMENT '%1'; \
    set-hook -t ${session_id} after-new-window 'send-keys \\\"conda activate '%1'\\\" Enter'; \
    set-hook -t ${session_id} after-split-window 'send-keys \\\"conda activate '%1'\\\" Enter' \" \
\"display \\\"No virtual env found for '%1'\\\"\" "
